function love.load()
	w = 1000
	h = 700
	u = 400
	length = 400

	world = love.physics.newWorld(w, h)
	world:setGravity(0, -700)
	world:setMeter(u)

	b_base = love.physics.newBody(world, w/2, h - 50, 0, 0)
	b_pendulum = love.physics.newBody(world, b_base:getX() - (length / math.sqrt(2)) , b_base:getY() - (length / math.sqrt(2)), 20, 0)
	s_pendulum = love.physics.newCircleShape(b_pendulum, 0, 0, 10)
	joint = love.physics.newDistanceJoint(b_base, b_pendulum, b_base:getX(), b_base:getY(), b_pendulum:getX(), b_pendulum:getY())

	b_ground = love.physics.newBody(world, w/2, b_base:getY() + 20, 0, 0)
--	s_ground = love.physics.newRectangleShape(b_ground, -w/2, 0, w, 20)

	love.graphics.setMode(w, h, false, true, 0)

	last_vx = 0
	max_deflection = 0
	deflection = 0
end

function love.update(dt)
	world:update(dt)
	angle = math.atan((b_pendulum:getX() - b_base:getX()) / (b_pendulum:getY() - b_base:getY()))
	if b_base:getY() < b_pendulum:getY() then
		angle = angle + math.pi
	end

	if j then
		j:setTarget(love.mouse.getPosition())
		l = getLength(b_base:getX(), b_base:getY(), love.mouse.getPosition())
		if l > length then
			joint:setLength(length)
		else
			joint:setLength(l)
		end
		deflection = 0
		max_deflection = 0
	else
		vx = b_pendulum:getLinearVelocity()
		if last_vx * vx < 0 then
			deflection = math.abs(angle) * 180 / math.pi
			if max_deflection < deflection then
				max_deflection = deflection
			end
		end
		last_vx = vx
	end
end

function getLength(x1, y1, x2, y2)
	return math.sqrt((x1-x2)^2 + (y1-y2)^2)
end

function love.draw()
	love.graphics.polygon("line", b_base:getX() - 100, b_base:getY() + 20, b_base:getX() - 30, b_base:getY() - length - 20, b_base:getX() + 30, b_base:getY() - length - 20, b_base:getX() + 100, b_base:getY() + 20)

	love.graphics.setLineWidth(2)
	love.graphics.setColor(80,255,80)
	love.graphics.line(b_base:getX(), b_base:getY(), b_base:getX() - length * math.sin(angle), b_base:getY() - length * math.cos(angle))

	love.graphics.setColor(255,255,255)
	love.graphics.setLineWidth(1)
	love.graphics.circle("fill", b_pendulum:getX(), b_pendulum:getY(), 10, 20)

	love.graphics.print(string.format("FPS: %d\nAngle: %.2f deg\nDeflection: %.2f deg\nMax deflection: %.2f deg", love.timer.getFPS(), angle * 180 / math.pi, deflection, max_deflection), 20, 30)
end

function love.mousepressed(x, y, btn)
	if btn == 'l' then
		j = love.physics.newMouseJoint(b_pendulum, b_pendulum:getPosition())
	end
end

function love.mousereleased(x, y, btn)
	if btn == 'l' and j then
		j:destroy()
		j = false
	end
end
