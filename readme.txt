Installation:
1. Install love2d engine (http://love2d.org)

Running:
1. Put main.lua in any directory (say, "metronome")
2. Run "love metronome"

OR
1. zip main.lua
2. Rename zip archive to "*.love" (say, "metronome.love")
3. Run "love metronome.love"
